# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=libgcrypt
pkgver=1.9.1
pkgrel=0
pkgdesc="general purpose crypto library based on the code used in GnuPG"
url="https://www.gnupg.org/"
arch="all"
license="LGPL-2.1-or-later"
depends_dev="libgpg-error-dev"
makedepends="$depends_dev texinfo"
subpackages="$pkgname-static $pkgname-dev $pkgname-doc"
source="https://www.gnupg.org/ftp/gcrypt/libgcrypt/libgcrypt-$pkgver.tar.bz2"

# secfixes:
#   1.8.5-r0:
#     - CVE-2019-13627
#   1.8.4-r2:
#     - CVE-2019-12904
#   1.8.3-r0:
#     - CVE-2018-0495

build() {
	local _arch_configure=
	case "$CARCH" in
	arm*)
		# disable arm assembly for now as it produces TEXTRELs
		export gcry_cv_gcc_arm_platform_as_ok=no
		;;
	x86 | x86_64)
		_arch_configure="--enable-padlock-support"
		;;
	esac

	./configure \
		--build=$CBUILD \
		--host=$CHOST \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-static \
		$_arch_configure

	make
}

check() {
	# t-secmem fails on ppc64le, see https://dev.gnupg.org/T3375
	[ "$CARCH" != ppc64le ] || return 0
	make check
}

package() {
	make DESTDIR="$pkgdir" install
	rm -f $pkgdir/usr/share/info/dir
}

static() {
	pkgdesc="$pkgname static libraries"
	mkdir -p "$subpkgdir"/usr/lib
	mv "$pkgdir"/usr/lib/*.a "$subpkgdir"/usr/lib/
}

sha512sums="e8a028724cf5476fff0ca82c5c279a64b3bc5d1fd1472b784df4084b185266825baffc49e27b90db7453c8faef68cd0b8264f379abacee629bbdf6b11f2a28d6  libgcrypt-1.9.1.tar.bz2"
